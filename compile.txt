# Compilation instructions
## Requirements
In order to compile this game, the following must be installed first:

* SDL v1.2 (version 2.0 and higher will NOT work!)
* SDL_image
* SDL_mixer
* zlib

Additionally, you need the following tools:
* a C compiler, for example GCC.
* GNU Make or a similar program.

## How to compile
### Under GNU/Linux
Run make in the same directory where you find the file called “Makefile”:

$ make

On success, this will create an executable file called “meritous” in
said directory. This is the game.

### Other operating systems
Sorry, you have to figure it out on your own for now.

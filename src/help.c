//
//   help.c
//
//   Copyright 2007, 2008 Lancer-X/ASCEAI
//
//   This file is part of Meritous Recharged.
//
//   Meritous Recharged is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   Meritous Recharged is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with Meritous Recharged.  If not, see <http://www.gnu.org/licenses/>.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <string.h>

#include "levelblit.h"

struct help_line {
	char *t;
};

struct help_section {
	int lines;
	char *identifier;
	struct help_line *l[256];
};

struct help_file {
	int sections;
	struct help_section *s[256];
};

struct help_file *hlp = NULL;
struct help_section *itemlist = NULL;
int my_line;
int my_sec;
int itemlistid;
int my_cursor;
int my_link;
int my_return;

void InitHelp()
{
	FILE *fp;
	struct help_section *current_sec = NULL;
	struct help_line *current_line = NULL;
	char linebuf[80];
	hlp = malloc(sizeof(struct help_file));
	hlp->sections = 0;
	
	fp = fopen("dat/d/helpfile.txt", "r");
	while (!feof(fp)) {
		if (fgets(linebuf, 79, fp) == NULL) {
			break;
		}
		if (linebuf[strlen(linebuf)-1] == 0x0A)
			linebuf[strlen(linebuf)-1] = 0;
			
		if (linebuf[0] == '\'') {
			// comment
			continue;
		}
		if (linebuf[0] == ':') {
			// section
			hlp->s[hlp->sections] = malloc(sizeof(struct help_section));
			current_sec = hlp->s[hlp->sections];
			hlp->sections++;
			current_sec->identifier = malloc(strlen(linebuf));
			current_sec->lines = 0;
			strcpy(current_sec->identifier, linebuf+1);
			continue;
		}
		
		// different line
		if (current_sec != NULL) {
			current_sec->l[current_sec->lines] = malloc(sizeof(struct help_line));
			current_line = current_sec->l[current_sec->lines];
			current_sec->lines++;
			current_line->t = malloc(strlen(linebuf)+1);
			strcpy(current_line->t, linebuf);
		}
	}
	fclose(fp);
}


int CheckItemlistSection(char* id)
{
	if (strcmp(id, "itemlist_header")==0) {
		return 1;
	}
	if (strcmp(id, "artifact_map")==0) {
		if (artifacts[0]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_shieldbooster")==0) {
		if (artifacts[1]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_extracrystalefficiency")==0) {
		if (artifacts[2]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_circuitbooster")==0) {
		if (artifacts[3]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_metabolismenhancer")==0) {
		if (artifacts[4]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_dodgeenhancer")==0) {
		if (artifacts[5]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_etherealmonocle")==0) {
		if (artifacts[6]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_crystalgatherer")==0) {
		if (artifacts[7]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_sword")==0) {
		if (artifacts[8]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_halberd")==0) {
		if (artifacts[9]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_bow")==0) {
		if (artifacts[10]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_seal")==0) {
		if (artifacts[11]) {
			return 1;
		} else {
			return 0;
		}
	}
	if (strcmp(id, "artifact_agateknife")==0) {
		if (player_shield == 30) {
			return 1;
		} else {
			return 0;
		}
	}
	return 0;
}

void InitItemlistSection()
{
	itemlist = malloc(sizeof(struct help_section));
	itemlist->identifier = malloc(strlen("itemlist"));
	itemlist->lines = 0;
	strcpy(itemlist->identifier, "itemlist");
	hlp->s[hlp->sections] = itemlist;
	itemlistid = hlp->sections;
	hlp->sections++;
}

void CreateItemlistSection()
{
	itemlist->lines = 0;
	int currentline = itemlist->lines;
	struct help_section *sec;
	struct help_section *no_artifacts = NULL;
	struct help_section *footer = NULL;

	int s, l;
	int sections = 0;

	if (on_title) {
		/* In main menu; just print an “excuse” message */
		for (s=0; s<hlp->sections; s++) {
			sec = hlp->s[s];
			if (strcmp(sec->identifier, "itemlist_mainmenu")==0) {
				for (l=0; l<sec->lines; l++) {
					itemlist->l[currentline] = sec->l[l];
					currentline++;
				}
				itemlist->lines = itemlist->lines + sec->lines;
			}
		}
	} else {
		/* In game */
		for (s=0; s<hlp->sections; s++) {
			sec = hlp->s[s];
			if (CheckItemlistSection(sec->identifier)) {
				for (l=0; l<sec->lines; l++) {
					itemlist->l[currentline] = sec->l[l];
					currentline++;
				}
				itemlist->lines = itemlist->lines + sec->lines;
				sections++;
			}
			if (strcmp(sec->identifier, "no_artifacts") == 0) {
				no_artifacts = sec;
			}
			if (strcmp(sec->identifier, "itemlist_footer") == 0) {
				footer = sec;
			}
		}
		/* If there’s only 1 section, then we only have the intro section
		and can conclude that there are no artifact sections */
		if(sections <= 1) {
			/* Print out special section for when no artifacts have
			been collected */
			for (l=0; l<no_artifacts->lines; l++) {
				itemlist->l[currentline] = no_artifacts->l[l];
				currentline++;
			}
			itemlist->lines = itemlist->lines + no_artifacts->lines;
		}
	
		/* Print footer */
		for (l=0; l<footer->lines; l++) {
			itemlist->l[currentline] = footer->l[l];
			currentline++;
		}
		itemlist->lines = itemlist->lines + footer->lines;
	}
}

void BlitArtifact(char* textptr, int i) {
	int a;
	SDL_Rect from, to;
	SDL_Surface* sprite;

	a = atoi(textptr+1);
	if(a != 12) {
		// Not Agate Knife
		from.x = a * 32;
		sprite = artifact_spr;
	} else {
		// Agate Knife
		from.x = 0;
		sprite = agate_knife_spr;
		if (agate_knife_spr == NULL) {
			agate_knife_spr = IMG_Load("dat/i/agate.png");
			SDL_SetColorKey(agate_knife_spr, SDL_SRCCOLORKEY | SDL_RLEACCEL, 0);
		}
	}
	from.w = 32;

	to.x = 40;
	to.y = 40+i*10+10;

	if(to.y < 35) {
		from.h = 32 - (35 - to.y);
		from.y = 32 - from.h;
		to.y = to.y + from.y;
	} else {
		from.y = 0;
		from.h = 32;
	}

	if(to.y > 413) {
		from.h = 32 - (to.y - 413);
	} else {
		from.h = 32;
	}

	if(from.h > 0 || from.h <= 32) {
		SDL_BlitSurface(sprite, &from, screen, &to);
	}
}

void DisplayHelp()
{
	static int tick = 0;
	int i;
	struct help_section *current_sec = NULL;
	char *ltext;
	char c_ident[20];
	int line_num;
	int follow_link = 0;
	char linkfollow[20] = "";
	
	DrawRect(23, 23, 594, 434, 0);
	DrawRect(24, 24, 592, 432, 200);
	DrawRect(25, 25, 590, 430, 255);
	DrawRect(26, 26, 588, 428, 200);
	DrawRect(27, 27, 586, 426, 100);
	DrawRect(30, 30, 580, 420, 20);
	DrawRect(35, 35, 570, 410, 60);
	
	// 70x40 display
	current_sec = hlp->s[my_sec];
	
	if (current_sec->lines > 40) {
		my_line = my_cursor - 19;
	} else {
		my_line = my_cursor - 39;
	}
	if (my_line < 0) my_line = 0;
	if (my_line >= (current_sec->lines)) my_line = current_sec->lines - 1;
	for (i = 0; i < 2; i++) {
		draw_text(23+i, 40+(my_cursor - my_line)*10, "->", 255);
		draw_text(599+i, 40+(my_cursor - my_line)*10, "<-", 255);
	}
	
	for (i = -5; i < 40; i++) {
		line_num = my_line + i;
		if (line_num >= 0) {
			if (line_num < current_sec->lines) {
				ltext = current_sec->l[line_num]->t;
				if (i >= 0) {
					switch (ltext[0]) {
						case '!':
						
							draw_text(40 + (560-strlen(ltext+1)*8)/2, 40+i*10, ltext+1, 255);
							break;
						case '?':
							strncpy(c_ident, ltext+1, strchr(ltext+1, '?')-ltext-1);
							c_ident[strchr(ltext+1, '?')-ltext-1] = 0;
							
							draw_text(40, 40+i*10, strchr(ltext+1, '?')+1, my_cursor == line_num ? 200+(tick%16)*3 : 150);
							if ((my_link == 1)&&(my_cursor == line_num)) {
								follow_link = 1;
								strcpy(linkfollow, c_ident);
							}
							break;
						case ';':
							draw_text(80, 40+i*10, ltext+1, 200);
							break;
						case ',':
							BlitArtifact(ltext, i);
							break;
						default:
							draw_text(40, 40+i*10, ltext, 200);
							break;
					}
				} else {
					if (ltext[0] == ',') {
						BlitArtifact(ltext, i);
					}
				}
			}
		}
	}
	tick++;
	SDL_UpdateRect(screen, 0, 0, 0, 0);

	if (my_return) {
		my_sec = 0;
		my_cursor = 0;
		my_return = 0;
	}
	
	if (follow_link) {
		for (i = 0; i < hlp->sections; i++) {
			if (strcmp(linkfollow, hlp->s[i]->identifier) == 0) {
				my_sec = i;
				my_cursor = 0;
				break;
			}
		}
		my_link = 0;
	}
}

int MoveCursor()
{
	SDL_Event ev;
	static int key_delay = 0;
	static int key_up = 0, key_down = 0;
	static int joy_up = 0, joy_down = 0;
	int lastline = hlp->s[my_sec]->lines-1;
	
	if (key_delay > 0) key_delay--;
	
	my_link = 0;
	while (SDL_PollEvent(&ev)) {
		if (ev.type == SDL_KEYDOWN) {
			if (ev.key.keysym.sym == SDLK_DOWN) {
				key_down = 1;
				key_delay = 10;
				if (my_cursor < lastline) my_cursor++;
			}
			if (ev.key.keysym.sym == SDLK_UP) {
				key_up = 1;
				key_delay = 10;
				if (my_cursor > 0) my_cursor--;
			}
			if (ev.key.keysym.sym == SDLK_PAGEDOWN) {
				my_cursor = my_cursor + 40;
				if (my_cursor > lastline) {
					my_cursor = lastline;
				}
			}
			if (ev.key.keysym.sym == SDLK_PAGEUP) {
				my_cursor = my_cursor - 40;
				if (my_cursor < 0) {
					my_cursor = 0;
				}
			}
			if (ev.key.keysym.sym == SDLK_HOME) {
				my_cursor = 0;
			}
			if (ev.key.keysym.sym == SDLK_END) {
				my_cursor = lastline;
			}
			if (ev.key.keysym.sym == SDLK_BACKSPACE) {
				my_return = 1;
			}
			if (ev.key.keysym.sym == SDLK_ESCAPE) {
				return 0;
			}
			if (ev.key.keysym.sym == SDLK_h || ev.key.keysym.sym == SDLK_F1) {
				return 0;
			}
			if (ev.key.keysym.sym == SDLK_i) {
				return 0;
			}
			if ((ev.key.keysym.sym == SDLK_SPACE) || (ev.key.keysym.sym == SDLK_RETURN))
				my_link = 1;
			}

		if (ev.type == SDL_KEYUP) {
			if (ev.key.keysym.sym == SDLK_DOWN) {
				key_down = 0;
			}
			if (ev.key.keysym.sym == SDLK_UP) {
				key_up = 0;
			}
		}
		if (ev.type == SDL_JOYHATMOTION) {
			if (ev.jhat.value & SDL_HAT_DOWN) {
				joy_down = 1;
				key_delay = 10;
				if (my_cursor < lastline) my_cursor++;
			} else {
				joy_down = 0;
			}
			if (ev.jhat.value & SDL_HAT_UP) {
				joy_up = 1;
				key_delay = 10;
				if (my_cursor > 0) my_cursor--;
			} else {
				joy_up = 0;
			}
		}
		if (ev.type == SDL_JOYBUTTONDOWN) {
			if (ev.jbutton.button == 1) {
				my_link = 1;
			}
			if (ev.jbutton.button == 3 || ev.jbutton.button == 4 || ev.jbutton.button == 6) {
				return 0;
			}
		}
		if (ev.type == SDL_QUIT) {
			return 0;
		}
	}
	
	if (key_delay == 0) {
		if (key_up == 1 || joy_up == 1) {
			if (my_cursor > 0) my_cursor--;
		}
		if (key_down == 1 || joy_down == 1) {
			if (my_cursor < hlp->s[my_sec]->lines-1) my_cursor++;
		}
	}
	
	return 1;
}


void ShowHelpWindow(int start_section)
{
	int in_help = 1;
	if (hlp == NULL) {
		InitHelp();
	}
	if (itemlist == NULL) {
		InitItemlistSection();
	}
	CreateItemlistSection();
	my_sec = start_section;
	my_line = 0;
	my_cursor = 0;
	my_link = 0;
	my_return = 0;
	
	while (in_help)
	{
		DisplayHelp();
		in_help = MoveCursor();
		SDL_Delay(30);
	}
}


void ShowHelp()
{
	ShowHelpWindow(0);
}

void ShowItemlist()
{	if (hlp == NULL) {
		InitHelp();
	}
	if (itemlist == NULL) {
		InitItemlistSection();
	}
	ShowHelpWindow(itemlistid);
}

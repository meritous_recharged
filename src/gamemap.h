//
//   gamemap.h
//
//   Copyright 2007, 2008 Lancer-X/ASCEAI
//
//   This file is part of Meritous Recharged.
//
//   Meritous Recharged is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   Meritous Recharged is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with Meritous Recharged.  If not, see <http://www.gnu.org/licenses/>.
//

// Exposes gamemap.c functionality and types

#ifndef GAMEMAP_H
#define GAMEMAP_H

void RecordRoom(int room_id);
void DisplayAutomap();
void InitAutomap();

extern int c_scroll_x, c_scroll_y;

#endif


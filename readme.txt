Meritous Recharged v1.3
for Windows 98, Windows 2000, Windows XP, GNU/Linux
(and possibly other operatings systems)

by Lancer-X/ASCEAI
and, since v1.3, Wuzzy.

INTRODUCTION:
Far below the surface of the planet is a secret.     
A place of limitless power. Those that seek to
control such a utopia will soon bring an end        
to themselves.                       

Seeking an end to the troubles that plague him,
Psi user Merit journeys into the hallowed Orcus
Dome in search of answers.


IN-GAME-HELP and GAMEPLAY:
Meritous Recharged has a pretty exhaustive in-game help, which can be
accessed at almost any time by pressing the [H] key or in the main menu. 
The in-game help contains everything you need to know about the
gameplay.
This readme file shall only list mostly technical notes about the game.


INSTALLATION:
On Windows:

Run Meritous.exe after extraction and everything should work correctly.

If the game is too difficult for you, you can play in 'Wuss mode' for
a far more sombre gaming experience.

On GNU/Linux, a BSD system, or a similar, UNIX-like system:

Run the file meritous_recharged. For now, this game can only be run
“in place”, which means the folder structure around the meritous file
should be intact. There are no install scripts for now. Sorry.


COMPILATION:
See compilation.txt.



BASIC CONTROLS:
The game can be controlled with keyboard or joystick/gamepad.
Joystick support is slightly experimental for now and only the first
connected joystick will be recognized.
		
Keyboard	Joystick  Action
Arrow keys 	Hat	  Move around.
			  To walk through doors, simply walk up to them
			  and push against them.

Space		Button 0  Charge your psi circuit for attacking.

Enter		Button 1  Activate a trigger tile that you are standing on.
			  Enter is also used for various other things, such as
			  for reading in-game dialogue.

Tab        	Button 2  View the map
			  (You can then use the arrow keys to scroll around the
			  map. Hold the charge key to scroll faster)

H		Button 3  View the help file.

I          	Button 4  View the list of collected artifacts.

P		Button 5  Pause the game.

F2		N/A	  Toggle sound effects.

F3		N/A	  Toggle music.

Escape		Button 6  Exit the game.


COMMAND-LINE ARGUMENTS:
You can provide these command-line arguments, seperated by spaces, in any order:

fullscreen	Starts the game in full-screen mode (window mode by default).
nointro		Skips the intro. You may also just hit a key to skip it.
nosounds	Starts with sound effects disabled (can be re-enabled with [F2])
nomusic		Starts with music disabled (can be re-enabled with [F3])


COMMENTS:
You can send comments/questions/whatever to my email address, which is
given on the website.


ACKNOWLEDGEMENTS:
Thanks to Asceai, who created the original Meritous, the game from which
Meritous Recharged has been derived.
Thanks to the beta testers Quasar, Terryn and Wervyn, for their hard work
in assisting in keeping Meritous fairly stable.
